#!/bin/bash

case "$1" in
  macos)
    $0 brew
    $0 packages macos
    $0 link
    ;;
  packages)
    case "$2" in
      macos)
      	brew bundle --file='install/Brewfile'
        ;;
      linux-dev)
        ;;
      linux-server)
        ;;
      *)
        echo $"Usage: $0 $1 {macos|linux-dev|linux-server}"
        exit 1
        ;;
      esac
    ;;
  brew)
    type brew &>/dev/null
    if [ $? != "0" ]; then
      /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi
    ;;
  link)
    stow -t ~ zsh extra git hg tmux
    ;;
  relink)
    stow -t ~ -R zsh extra git hg tmux
    ;;
  unlink)
    stow -t ~ -D zsh extra git hg tmux
    ;;
  *)
    echo $"Usage: $0 {macos|packages|brew|link|relink|unlink}"
    exit 1
    ;;
esac
